﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AliExpressPoc
{
    class ProductDetails
    {
        public int SrNo { get; set; }
        public string Title { get; set; }
        public string ImgUrl { get; set; }
        public string ProductDescription { get; set; }
        public string Price { get; set; }
        public string DiscountedPrice{ get; set; }
        public string Weight { get; set; }
        public string size { get; set; }
        public string StarRating { get; set; }
        public string NoOfOrders { get; set; }
    }
}
