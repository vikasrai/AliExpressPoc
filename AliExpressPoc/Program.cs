﻿using AliExpressPoc.AppCode;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;

namespace AliExpressPoc
{
    class Program
    {
        static System.Timers.Timer TimerWatch = new System.Timers.Timer();
        static void Main(string[] args)
        {
            TimerWatch.Interval = 10000; //every 1 mins
            TimerWatch.Elapsed += new System.Timers.ElapsedEventHandler(Timer_Tick);
            TimerWatch.Enabled = true;
            Console.WriteLine(DateTime.Now);
            MainProcess();
            Console.WriteLine(DateTime.Now);
            while (Console.ReadLine() != "exit") ;
            //Console.Write("press any key to exit...");
            //Console.ReadKey();
        }

        private static void MainProcess()
        {
            List<ProductDetails> Data1 = new List<ProductDetails>();
            List<ProductDetails> Data2 = new List<ProductDetails>();
            try
            {
                List<string> FileNames = GetFilesName();
                var UrlsFromCsv = ProcessCsv.ReadUrl(FileNames); //this method is used to read url from csv file

                if (UrlsFromCsv.Count == 0) // check if file not found or file is empty
                {
                    Console.WriteLine("Url not found in csv file");
                    Log.Write("Url not found in csv file");
                    Console.WriteLine("---------------------------------------------------------------");
                    flag = true;
                    return;
                }
                var Urls = BreakListByTwo<string>(UrlsFromCsv);//this method is used to break the list

                Console.WriteLine("System starts extracting product details...");
                Log.Write("System starts extracting product details...");

                Thread childThread1 = new Thread(() => Data1 = ParseHtml.ParseHtmlThread(Urls[0]));
                childThread1.Start();

                Thread childThread2 = new Thread(() => Data2 = ParseHtml.ParseHtmlThread(Urls[1]));
                childThread2.Start();

                // wait until both the threads are completed their execution
                childThread1.Join();
                childThread2.Join();

                Console.WriteLine("System completes extracting product details.");
                Log.Write("System completes extracting product details.");

                var Data = Data1.Concat(Data2).ToList();// concating list into one

                for (int i = 0; i < Data.Count; i++)
                {
                    Data[i].SrNo = (i + 1); //serial no of the list is set here 
                }

                Log.Write("System starts writing product details into csv file...");
                Console.WriteLine("System starts writing product details into csv file...");

                ProcessCsv.WriteProductDetails(Data);// write to csv file

                Log.Write("System completes writing product details into csv file...");
                Console.WriteLine("System completes writing product details into csv file...");
                Log.Write("Csv file has been Exported.");
                Console.WriteLine("Csv file has been Exported.");
                Console.WriteLine("---------------------------------------------------------------");
            }
            catch (Exception Ex)
            {
                Log.Write("Error Occurred: " + Ex.Message);
                Console.WriteLine("Error Occurred: " + Ex.Message);
            }
            flag = true;
        }
        /// <summary>
        /// this method is used to break given list into two parts and return list of given list
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public static List<List<T>> BreakListByTwo<T>(List<T> source)
        {
            List<T> list1 = new List<T>();
            List<T> list2 = new List<T>();
            List<List<T>> data = new List<List<T>>();
            for (int i = 0; i < source.Count; i++)
            {
                if ((source.Count / 2) <= i)
                {
                    list1.Add(source[i]);
                }
                else
                {
                    list2.Add(source[i]);
                }
            }
            data.Add(list1);
            data.Add(list2);
            return data;
        }
        private static void Timer_Tick(object sender, ElapsedEventArgs e)
        {
            watch();
        }

        /// <summary>
        /// It is used for prevent folder watcher to raise event more than one
        /// </summary>
        public static bool flag = true; 
        /// <summary>
        /// this method is used to check if a new file is created into the folder
        /// </summary>
        private static void watch()
        {
            FileSystemWatcher watcher = new FileSystemWatcher();
            watcher.Path = AppDomain.CurrentDomain.BaseDirectory + @"\CSV\";
            watcher.NotifyFilter = NotifyFilters.FileName | NotifyFilters.DirectoryName | NotifyFilters.Size | NotifyFilters.LastWrite | NotifyFilters.CreationTime | NotifyFilters.LastAccess;
            watcher.Filter = "*.*";
            watcher.Created += new FileSystemEventHandler(OnCreated);
            watcher.EnableRaisingEvents = true;

        }
        private static void OnCreated(object source, FileSystemEventArgs e)
        {
            if (flag)
            {
                flag = false;
                Console.WriteLine(DateTime.Now);
                TimerWatch.Enabled = false;
                MainProcess();
                TimerWatch.Enabled = true;
                Console.WriteLine(DateTime.Now);
            }
        }

        /// <summary>
        /// this method is used to get all files from the folder
        /// </summary>
        /// <returns></returns>
        private static List<string> GetFilesName()
        {
            DirectoryInfo d = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory + @"\CSV\");
            FileInfo[] Files = d.GetFiles("*.csv"); //Getting csv files
            List<string> FileNames = new List<string>();
            foreach (FileInfo file in Files)
            {
                FileNames.Add(file.Name);
            }
            return FileNames;
        }
    }
}
